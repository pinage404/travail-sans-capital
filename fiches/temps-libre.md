---
title: Temps libre
description: Projets non-rémunérateurs financièrement.
sections:
- answers:
    Nope: [salariat, zoneEnnui]
    À la demande: [salariat]
    Fixé: [salariat]
    Discuté collectivement: [coop]
    Libre: [coop, micro-entreprise, portage salarial, CAE, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
Ce temps que tu passes en moins au travail, qu'est-ce que ça t'apporte en plus ?

**L'équilibre entre ce que ça m'apporte, ce que ça apporte au monde par rapport à ce que ça me coûte**.\
Aux 35h, tout ton temps est structuré pour le travail — le temps qui reste est organisé pour que tu sois en forme pour le travail (ou alors tu tiens les drogues dans la durée, ou tu te reposes au travail le lundi matin).

- Framasoft
- Contribulle
- NousToutes
- NUPES
- Plan Climat Dinan Agglo
- Siestes
- Garder une marcassine
- Aller chez le médecin
- Donner des confs
- Flus
- Scribouilli
- Lire
- Apprendre à faire du vélo
- Commission locale de l'eau
- Bouquiner au soleil
- Aller marcher
- Aider les copain·es pour leur plan climat
- Planter des arbres chez un maraicher
- Jouer à Zelda
- Participer à des ateliers
- Animer des ateliers
- Bénévolat et militantisme
- S'occuper de "chez soi"
- Apprendre

Et toi ?
