---
title: Accompagnement dans mon activité
sections:
- answers:
    Inexistant / À aller chercher: [salariat, portage salarial, coop, micro-entreprise, zoneProbleme]
    Possible: [salariat, coop, zoneEnthousiasme]
    Adapté à mes besoins: [CAE, zoneEnthousiasme]
tags:
- clarifier
---
