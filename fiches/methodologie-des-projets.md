---
title: Méthodologie des projets
sections:
- answers:
    « On fait de l'agile » = Des estimations: [zoneEnnui]
    Process imposé: [zoneEnnui]
    Y'a une personne qui porte tout: [zoneProbleme]
    Ça manque de méthode: [zoneEnnui]
    L'équipe sait s'adapter et décider des priorités: [zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
