---
title: Humour
sections:
- title: Ambiance
  answers:
    Pénible / On m'enfonce: [zoneDanger]
    Y a des « Petites blagues »: [zoneProbleme]
    Pas pire: []
    Tout le monde prend soin: [zoneEnthousiasme]
- title: Réaction à «C'est pas drôle»
  answers:
    J'peux rien dire: [zoneDanger]
    « Ça va, c'est juste pour rire » / Minimisation du problème / Pas de remise en question: [zoneDanger]
    Excuses rapides: [zoneProbleme]
    Excuses sincères: []
tags:
- évaluer
- clarifier
---
