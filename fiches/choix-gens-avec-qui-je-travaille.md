---
title: Choix des gens avec qui je travaille
sections:
- answers:
    Patrons: [salariat, zoneEnnui]
    Moi: [micro-entreprise, portage salarial, coop, CAE, zoneEnthousiasme]
    Équipe: [coop]
related_to:
- seule-ensemble.md
tags:
- évaluer
- clarifier
---

Ouvrir à "avec qui je travaille, dans d'autres structures".

On peut choisir :
- des gens avec qui c'est pas pénible de travailler
- des gens avec qui tu prends du plaisir à travailler

Les "coups de foudre professionnels". "C'est tellement agréable, c'est limpide, c'est évident."\
C'est un continuum des clients.

Mes collègues idéal, c'est des gens avec qui je travaille. C'est pas forcément des gens de ma structure.

Des personnes avec qui on peut creuser un sujet ensemble + loin que tout seul

On peut aussi progresser d'un point de vue interpersonnel, échanges francs "là t'as fait tel truc je pense que ça a gêné telle chose "Pas que dans la technique, passer du savoir-faire au savoir-être.

Et ptet qu'on a tendance à aller dans des structures ou il y a bcp de savoir-faire au détriment du savoir-être. Or le savoir-être c'est souvent un enjeu, là ou le savoir faire on peut se débrouiller, apprendre…

Principes de recrutement rarement basés sur le savoir-être. Et nos organisations nous permettent de tester pdt qques jours puis de vouloir rebosser ensemble + longuement ensuite.

Coops permet de brasser un peu plus, de piquer des trucs d'un projet à un autre… évite la monoculture.

C'est facile de bosser avec des gens qui sont dans l'écoute, dans la compréhension, dans la remise en question, dans la verbalisation des trucs qui vont bien et qui n'ont pas de sens. Dédicace à …
