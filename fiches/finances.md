---
title: Finances
sections:
- title: Visibilité
  answers:
    Opaque: [salariat, zoneDanger]
    Partielle: [salariat, CAE, coop, zoneProbleme]
    Totale:
      choices: [micro-entreprise, portage salarial, coop, CAE, zoneEnthousiasme]
      description: Moi + la structure
- title: Gestion
  answers:
    Individuelle: [portage salarial, micro-entreprise, CAE]
    Collective: [coop, CAE]
    Pas à m'en occuper: [coop, salariat]
- title: Aisance des conversations avec l'argent
  answers:
    Au secours: [zoneDanger]
    Mon stress augmente quand la caisse se vide: [zoneProbleme]
    OK, sans plus: [zoneEnnui]
    À l'aise: [zoneEnthousiasme]
tags:
- clarifier
---

Pour réduire la peur, ça m'a aidé de savoir de quoi j'avais besoin pour vivre. C'est un truc très flou pour plein de personnes… De quoi j'ai besoin / j'ai vraiment besoin pour vivre -> si je réduis au minimum, évaluer mon besoin vital pour payer le loyer (ou déménager ?) / me nourrir / se chauffer (LOL)

Ca veut dire qu'il faut facturer combien pour se payer ce salaire sans perdre d'argent.

Maïtané : 1550€ salaire : Facturer 2500 / mois : 4 jours de facturation / mois (TJM 600)

Thomas : 2000€ salaire : Facturer 5000 / mois : 8 jours de facturation / mois (TJM 600 ou 700)

C'est le premier truc par lequel commencer pour se déstresser.
Ça donne du tangible.

https://libelilou.github.io/2017/05/29/sudweb.html

Point : On démarre pas en dessous de 350€ de TJM (surtout vu l'inflation) et moi j'ai démarré à 450 avec une petite année d'xp.

Et on peut faire fluctuer son salaire ! Réduire son salaire, se mettre au chômage (6 mois de cotiz), chômage partiel en cas de problème structurel (quasi-SMIC pris en charge quelques mois par l'État).

Il faut un peu de temps pour atteindre un "cycle régulier", ça ne se fait pas en 2 mois. Surveiller les sous et anticiper l'arrêt cardiaque (combien de temps je tiens avant de mettre la clé sous la porte en facturant au même rythme, voire 0).
