---
title: Formation et apprentissages
sections:
- answers:
    J'apprend rien: [salariat, zoneProbleme]
    À la demande: [salariat, coop, zoneEnnui]
    Fixé: [salariat, coop]
    Décidé collectivement: [coop, zoneEnthousiasme]
    Libre: [coop, portage salarial, CAE, micro-entreprise, zoneEnthousiasme]
    Ensemble: [coop, portage salarial, CAE, micro-entreprise, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
