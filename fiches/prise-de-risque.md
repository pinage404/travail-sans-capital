---
title: Prise de risque
sections:
- title: Si j'ai un pépin…
  answers:
    Si j'ai un coup dur, je suis à la rue: [zoneDanger]
    J'aurai besoin d'aide: [zoneProbleme]
    Je me débrouillerai: []
    Pas de souci: [zoneEnthousiasme]
tags:
- clarifier
---

_Récap d'où trouver de la ressource, et où évaluer les endroits où on peut se casser la gueule._

Est-ce que je peux demander de l'aide/trouver des ressources auprès de gens ?
- Couple
- Parent(s)
- Ami·es
- Famille

Train de vie / besoins fonctionnels :
- Loyer / prêt
- …

Est-ce que des personnes dépendent de moi :
- enfant(s)
- conjoint·e
- famille
- proche
- groupe de travail/association

Réseau :
- est-ce qu'il y a des clients là où je vis ?
- est-ce que je connais des personnes qui sauraient me recommander/coopter/me présenter aux bonnes personnes ?

Prise de recul sur l'activité à lancer :
- Dev web : OK
- Planter des petits pois en ville : beaucoup d'inconnues

Investissements :
- est-ce que des investissements sont nécessaires pour se lancer ?
- …
