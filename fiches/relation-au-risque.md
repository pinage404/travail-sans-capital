---
title: Relation au risque
sections:
- answers:
    Le déni = « Tout va bien »: [coop, salariat, micro-entreprise, CAE, zoneDanger]
    On peut pas faire comme ça = Fausse sécurité: [coop, salariat, portage salarial, micro-entreprise, CAE, zoneProbleme]
    Risques clairs et expliqués: [coop, portage salarial, micro-entreprise, CAE, zoneEnthousiasme]
    Risques anticipés: [CAE, coop, micro-entreprise, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
