---
title: Niveau d'énergie
sections:
- answers:
    Je suis en burn-out: [zoneDanger]
    J'ai envie de rien: [zoneDanger]
    Bof: [zoneProbleme]
    Ca dépend des jours: [zoneEnnui]
    Ca va: [zoneEnnui]
    Je suis en forme:
    Je pète la forme !: [zoneEnthousiasme]
---
