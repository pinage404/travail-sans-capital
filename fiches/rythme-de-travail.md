---
title: Rythme de travail
sections:
- answers:
    Temps plein sinon rien: [salariat, zoneProbleme]
    Adaptations possibles: [salariat, coop]
    Adaptations faciles: [coop, zoneEnthousiasme]
    Libre: [coop, micro-entreprise, portage salarial, CAE, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
Qui bosse 35-39h ici ? On est d'accord que c'est beaucoup trop ?! Je sais pas comment vous faites…

Ca fait 8 ans que je suis à 4j/semaine et je fais du mieux que je peux pour que ça reste comme ça.
Grande différence quand je suis passé à 4j, j'étais + vite reposé, en meilleure forme pour le taf, pas une journée où tu traines les pieds pour y aller.

Ca m'est arrivé de bosser 1/2j de plus et ça teintait le reste de la journée, de la semaine.

Si tu veux aller te balader en semaine, tu peux te barrer à 16h, dire "bon le taf peut attendre", choisir de te positionner sur ce qui te fait du bien.

Lié à comment tu bosses avec tes clients. Si t'as la pression, obligé de te presser, tu peux pas t'occuper toi… et quand tu commences à détricoter ça, tu peux écouter ton rythme, pas bosser quand t'es fatigué, cravacher quand t'en as envie parce que tu peux te reposer ensuite.

"Tu peux pas travailler 4 jours" ben pourquoi ?

Je suis une thérapie, faut que j'aille à Rennes. C'est à 1h15 de bus. Ça me prend une demi-journée. C'est du temps primordial. Grâce à ça j'ai moins peur des utilisateurs — c'est pratique dans mon boulot ! Ça me permet d'aller mieux. Si on compte le nombre de jours de taf que j'ai perdu parce que j'en n'avais pas pris soin…
---> Permet d'être + autonome, d'aller mieux
