---
title: Qui choisit les projets/clients ?
sections:
- answers:
    Patrons actionnaires: [salariat, zoneEnnui]
    Patrons: [salariat, zoneEnnui]
    Moi: [micro-entreprise, portage salarial, CAE, coop, zoneEnthousiasme]
    Conseil d'Administration: [coop]
    Tous·tes les travailleur·ses: [coop, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---

Expliciter ce qui est possible

- On ouvre les possibles
- Le sens
- La liberté de partir

D'ailleurs, on teste souvent quelques heures/jours au début pour savoir si on arrive à bosser ensemble. On ne s'engage pas sur 8 mois. Si ça ne va pas, je pars. C'était dur à dire mais ça m'a soulagé. Y'a pas de culpabilisation.

Choix mutuel : On bosse ensemble, l'un a l'argent et les idées, l'autre le savoir-faire technique.

Invitation à être à l'écoute de comment ça se passe ensemble, pas juste "Le contrat est signé, faut aller jusqu'au bout".

Ptet que ça autorise aussi des choses sur le rythme de travail ensuite… sur la qualité.

Choisir autrement ces clients (clients plaisir), la relation avec des clients peut devenir un truc travaillé, voire même un objet de travail. On est en co-responsabilité de la relation de travail, c'est pas juste "la faute au chef·fe de projet".

Sortir du paradigme du "client est roi". Subordination implicite.

Choisir le domaine du client. Je pourrais dire que je ne bosse pas pour l'armée — mais faire des cartes marines, je ne vois pas comment ça peut être un impact négatif de "mieux outiller la navigation maritime". C'est différent que de designer des interfaces pour les bateaux de guerre.
