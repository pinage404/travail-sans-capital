---
title: Bien choisir ses frites
draft: true
sections:
- question: Tu les cuis plutôt comment ?
  answers:
    Végétaline: [CAE, coop]
    Graisse de canard: [salarié, coop]
    Sans rien: [portage salarial, micro-entreprise]
- question: Des extravagances
  answers:
    Vinaigre: [portage salarial, micro-entreprise]
---

Pourquoi c'est important ?
- le gras ça réchauffe
- c'est convivial
- un lieu de travail sans frite, c'est un lieu de travail sans vie

Des pièges à éviter :
- les manger molles
- les manger le lendemain, froides
