---
title: Seul·e / à plusieurs
sections:
- answers:
    Le plus solo possible: [micro-entreprise, portage salarial]
    _Le plus solo possible: [micro-entreprise, portage salarial, CAE]
    _Jamais seul·e / toujours en groupe: [micro-entreprise, portage salarial, salariat, coop, CAE]
    Jamais seul·e / toujours en groupe: [micro-entreprise, portage salarial, salariat, coop]
tags:
- clarifier
---
Structure ou t'es seul·e, ou une structure ou il y a un peu/des/plein de gens ?

Faut avoir l'opportunité de tomber sur les gens avec qui tu peux faire des trucs.

Tu peux trouver une formule qui te va bien.

> Moi j'ai envie de bosser avec des gens, j'ai pas envie de bosser pour un patron.

Combien de fois on va se barrer d'une boîte parce qu'on est pas d'accord avec 1 personne à l'intérieur ?

Y'a un mode de structure pour toutes les envies, on a le choix.
