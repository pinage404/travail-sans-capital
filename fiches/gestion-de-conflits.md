---
title: Gestion des conflits
sections:
- answers:
    Ce que je dis sera retenu contre moi: [salariat, zoneDanger]
    Mon n+1 aura toujours raison: [salariat, portage salarial, zoneDanger]
    On évite d'en parler: [salariat, portage salarial, CAE, coop, zoneProbleme]
    On essaie mais on se comprend pas: [salariat, portage salarial, CAE, coop, zoneProbleme]
    On s'écoute et on prend soin: [CAE, coop, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
