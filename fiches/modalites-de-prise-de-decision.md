---
title: Modalités de prise de décision
sections:
- answers:
    Unilatéral: [salariat, zoneProbleme]
    _Unilatéral: [salariat]
    _Les concerné·es décident: [CAE, coop]
    Les concerné·es décident: [micro-entreprise, portage salarial, CAE, coop, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---

Si on a une discussion, on fait avancer le scénario ensemble, tandis qu'une décision unilatérale y'a pas de garde-fous, pas de façon de voir ses angles morts

Décision unilatérale bénéficie surtout à la personne qui en retire le profit, et si elle est en minorité dans la structure.

La différence : quand c'est unilatéral, c'est celui qui a l'argent/pouvoir qui décide, éventuellement ça des impacts positifs et/mais personne ne (peut) pose de questions. Sinon via la discussion/intelligence collective.

**Est-ce qu'on peut avoir une conversation**, indépendamment de qui décide ?  Conversation pour vérifier si il y a des risques. On fait tout à la conversation et à l'énergie du moment. C'est lié à la taille (petite).

Pour répondre à un appel d'offre, comme ça engage la CAE il faut demander Comment faire un truc (et pas forcément l'autorisation). Demande prise en compte du timing de l'équipe d'accompagnement.
