---
title: Accès à l'information
sections:
- answers:
    LOL: [salariat, zoneProbleme]
    Partielle / Procédurale: [salariat, CAE, portage salarial, zoneProbleme]
    Accessible mais galère / Faut trouver à qui demander: [salariat, coop, CAE, portage salarial, zoneProbleme]
    Totale: [coop, CAE, portage salarial, zoneEnthousiasme]
    Co-écrite: [coop, CAE, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
