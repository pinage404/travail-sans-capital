---
title: Multi activité
sections:
- answers:
    Grave possible (domaines différents): [CAE, coop, zoneEnthousiasme]
    Faisable (temps partiels): [coop, salariat]
    Possible dans le même domaine: [coop, salariat]
    Pas possible: [salariat, portage salarial, micro-entreprise]
tags:
- clarifier
---
