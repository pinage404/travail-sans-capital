---
title: Congés / Jours chômés
sections:
- answers:
    Fixé en partie: [salariat, zoneEnnui]
    Sur validation: [salariat, zoneEnnui]
    Facile à poser: [salariat, coop, CAE, zoneEnthousiasme]
    On compte pas: [coop, CAE, micro-entreprise, portage salarial, zoneEnthousiasme]
- title: Impact sur la rémunération
  answers:
    Perte d'argent (sans solde): [salariat, coop, CAE, zoneEnnui]
    Pas d'impact: [coop, CAE, micro-entreprise, portage salarial, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
