import { select } from 'd3'
import { scaleOrdinal, scaleBand } from 'd3'
import { axisBottom } from 'd3'
import { range } from 'd3'
import { lab } from 'd3'

const removeZones = (key) => /^zone/.test(key) === false
const selectZones = (key) => /^zone/.test(key) === true
const isDarkMode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches

/**
 * On fait de notre mieux pour rendre ce graphique le plus accessible possible
 *
 * @see https://tink.uk/accessible-svg-line-graphs/
 * @see https://www.sarasoueidan.com/blog/accessible-data-charts-for-khan-academy-2018-annual-report/
 * @param {HTMLElement} root
 * @param {Object} data
 */
function drawChart (root, data) {
  const colorScheme = JSON.parse(root.dataset.colors)

  const swappedData = Object.entries(data).reduce((obj, [category, values]) => {
    (values?.choices || values || []).forEach(v => obj[v] ? obj[v].push(category) : obj[v] = [category])
    return obj
  }, {})

  const leftMargin = 100
  const rightMargin = 100
  const paddingLeft = 30
  const paddingBottom = 30
  const paddingTop = 30
  const paddingRight = 30
  const xMinWidth = 180
  const height = 200

  const width = root.clientWidth
  const xCounts = Object.keys(data).length
  const xWidth = width / xCounts

  const x = scaleOrdinal()
    .domain(Object.keys(data))
    .range(range(0, width - leftMargin - rightMargin, xWidth))

  const y = scaleBand()
    .domain(Object.keys(swappedData).filter(removeZones))
    .range([paddingTop, height - paddingBottom])
    .padding(0.2)

  function buildStructureBar ([key, values = []]) {
    const el = select(this)
    const { color: fillColor, label=key } = colorScheme.structureTypes[key] || { color: '#ccc', label: '' }
    const w = values.at(-1) === values.at(0) ? xMinWidth : x(values.at(-1)) - x(values.at(0))
    const isSingleValue = values.length === 1
    const xTransform = x(values.at(0)) + leftMargin - (isSingleValue ? (xMinWidth/2) : paddingLeft)

    el
      .attr("transform", `translate(${xTransform} ${y(key)})`)
      .attr('data-length', values.length)

    el
      .append('rect')
      .style("fill", fillColor)
      .attr('height', y.bandwidth())
      .attr('width', w + (isSingleValue ? 0 : paddingLeft + paddingRight))

    el
      .append('text')
      .text(label)
      .attr("fill", lab(fillColor).l < 60 ? 'white' : 'black')
      .attr('x', paddingLeft / 3)
      .attr('y', y.bandwidth() / 2)
      .style('text-anchor', 'start')
      .style('dominant-baseline', 'middle')
  }

  function buildPersonalZone ([key, values]) {
    const el = select(this)
    const w = (xWidth * values.length) + (x(values.at(0)) + leftMargin)> width ? x(values.at(-1)) - x(values.at(0)) : xWidth * values.length
    const { color: fillColor, label } = colorScheme.zones[key] || { color: '#ccc', label: '' }
    const labColor = lab(fillColor)

    el
      .attr("transform", `translate(${x(values.at(0)) + leftMargin} +0.5)`)

    el
      .append('rect')
      .style("fill", fillColor)
      .attr('height', height - paddingBottom)
      .attr('width', w)
      .append('text')
        .text(key)

        el
      .append('text')
      .text(label)
      .attr('x', paddingLeft / 3)
      .attr('y', paddingTop / 2)
      .attr("fill", isDarkMode ? labColor.brighter(.8) : labColor.darker(.5))
      .style('text-anchor', 'start')
      .style('dominant-baseline', 'middle')
  }

  const xAxis = axisBottom(x)
    .tickFormat((value) => value.startsWith('_') ? '' : value)

  const svg = select(root)

  svg.append('g')
    .attr("transform", `translate(${leftMargin}, 170)`)
    .call(xAxis)

  svg.selectAll('zone')
    .data(Object.entries(swappedData).filter(([key]) => selectZones(key)))
    .join('g')
    .attr('class', 'personal-zone')
    .each(buildPersonalZone)

  svg.selectAll('bar')
    .data(Object.entries(swappedData).filter(([key]) => removeZones(key)))
    .join('g')
    .attr('class', 'structure-type')
    .attr('role', 'row')
    .each(buildStructureBar)

}

document.addEventListener('DOMContentLoaded', () => {
  Array.from(document.querySelectorAll('.chart'))
    .forEach(node => {
      const text = node.querySelector('script[type="application/json"]').innerText
      const svg = node.querySelector('svg')
      const data = JSON.parse(text)

      drawChart(svg, data)
    })
})
