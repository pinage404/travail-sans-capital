const EleventyVitePlugin = require("@11ty/eleventy-plugin-vite");
const mdLib = require('markdown-it')()

module.exports = function(eleventyConfig) {
  eleventyConfig.addWatchTarget("./assets");
  // @see https://github.com/11ty/eleventy-plugin-vite/issues/16#issuecomment-1241225950
  eleventyConfig.setServerPassthroughCopyBehavior("copy")
  eleventyConfig.addGlobalData('layout', 'layouts/page.html')

  eleventyConfig.addGlobalData('colorScheme', {
    structureTypes: {
      'CAE': {
        color: '#3093cc',
      },
      'coop': {
        color: '#0356e6',
      },
      'micro-entreprise': {
        color: '#834eca',
      },
      'portage salarial': {
        color: '#ca4eb0',
      },
      'salariat': {
        color: '#d62020',
      },
    },
    zones: {
      'zoneDanger': {
        color: '#d8241f',
        label: 'Danger'
      },
      'zoneProbleme': {
        color: '#ff7f00',
        label: 'Problème'
      },
      'zoneEnnui': {
        color: '#7f7f7f',
        label: 'Ennui'
      },
      'zoneEnthousiasme': {
        color: '#24a221',
        label: 'Enthousiasme'
      },
    }
  })

  eleventyConfig.addCollection('fiches', collectionApi => {
    return collectionApi
      .getFilteredByGlob('fiches/**/*.md')
      .filter(page => page.data.title)
      .filter(page => !page.data.draft)
      .sort((a, b) => a.data.title.trim().localeCompare(b.data.title.trim()))
  })

  eleventyConfig.addFilter('md', text => mdLib.render(text || ''))

  eleventyConfig.addPlugin(EleventyVitePlugin, {
    viteOptions: {
      publicDir: 'assets'
    }
  });
};
