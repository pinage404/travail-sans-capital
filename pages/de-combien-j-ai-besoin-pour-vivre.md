---
---

# De combien j'ai besoin pour vivre ?

C'est une des premières questions blocantes lorsque l'on veut se "mettre à son compte" ou juste négocier son salaire : De combien j'ai besoin pour vivre ?

## Étape 0 : Ca fait flipper de parler d'argent !!

Si jamais vous n'êtes pas à l'aise avec le fait de parler d'argent, plutôt que de faire les calculs vous-même, vous pouvez vous faire accompagner :
- d'un·e proche
- d'une CAE (si si, iels sont là pour vous accompagner !)

Mais si vous avez envie de rentrer dans le concret des chiffres, c'est par ici !

## Étape 1 : Calculer ses dépenses personnelles

On vous a préparé une petite liste, et nous vous invitons à la remplir afin de déterminer à peu près de combien d'euros sonnants et trébuchants vous avez besoin aujourd'hui.

Avant de continuer les calculs, on vous conseille de remplir la grille :

- La [version liste simple](/pages/budget-perso)
- Le [tableur déjà tout prêt](https://framagit.org/miette/travail-sans-capital/-/raw/main/ressources/tableur_budget_perso.ods)

Une fois rempli, **vous aurez normalement la somme qui doit représenter votre salaire net** (après impôts), ce qui doit tomber dans votre poche à la fin du mois

## Étape 2 : Savoir combien ça fait en coût total employeur

Pour cela, on vous conseille de vous rendre sur [le simulateur de l'URSSAF](https://mon-entreprise.urssaf.fr/simulateurs/salaire-brut-net)

Vous rentrez dans la case du bas votre salaire espéré après impôt, et vous regardez la case "Coût total employeur" pour savoir combien cela vous coûtera a minima.

Par exemple, au 1er novembre 2022, si je veux toucher le SMIC, il faut faire rentrer 1750€ /mois.

Maintenant vous avez 2 nombres :
- Votre **salaire net après impôts** (étape 1)
- Le **coût total employeur** de ce salaire (étape 2)

## Étape 3 : Penser aux risques professionnels

Si vous êtes à votre compte, il ne suffit pas de savoir combien doit tomber dans votre poche à la fin du mois ! Il faut aussi anticiper un certain nombre de choses, parmis lesquelles :

- Une baisse subite et subie d'activité
- Le rachat de nouveau matériel
- Les congés et arrêts longue durée
- Tout le temps non facturable, à gérer de l'administratif, des discussions avec de potentiels clients
- Le temps de formation…

Version rapide : Si on veut faire les choses grossièrement, on peut se dire qu'il faut compter 1 000€/mois d'économies pour être tranquille.

Pour voir ça dans le détail on vous conseille de lire :
- [l'article de iergo](https://blocnotes.iergo.fr/articles/comprendre-le-tjm-dun-freelance/)
- [l'article et surtout le tableur de Éric](https://n.survol.fr/n/dis-tonton-pourquoi-est-ce-si-cher-un-independant%E2%80%AF-v2) pour vous permettre de faire des calcul un peu fins.

Vous pouvez donc rajouter vos 1000€ d'économies à la somme issue du simulateur. Pour le SMIC (au 1er nov 2022) on en est donc à 1750€ + 1000€ = 2750€ / mois


Vous avez maintenant 3 nombres :
- Votre **salaire net après impôts** (étape 1)
- Le **coût total employeur** de ce salaire (étape 2)
- La **somme d'argent que vous devez facturer mensuellement**, qui est la somme du coût total employeur + les économies pour faire face au risque (environ 1000€/mois)

## Étape 4 : Choisir son tarif-jour

Maintenant que vous savez quelle somme vous devez faire rentrer chaque mois pour être confortable financièrement, il reste une étape : choisir son tarif-jour (petit nom raccourci: TJM).

### Le minimum : 400€ / jour

A minima, ne vous vendez pas en dessous de 400€ / jour, cela ne vous permettrait pas de vivre correctement et entraînerait un risque pour vous (vous n'aurez pas le temps de chercher des contrats satisfaisants, vous aurez des clients qui chercheront des tarifs bas et non pas de bonnes compétences, qui essaieront peut-être de négocier vos tarifs…). De plus, si vous êtes à un tarif très bas, vous tirerez toute votre profession vers le bas, et cela n'est gagnant pour personne à terme.

Pour mieux comprendre, on vous conseille [la super conférence-éclair de Libellule](https://libelilou.github.io/2017/05/29/sudweb.html)

### On ne facture pas 5 jours sur 7 !

Si on suit la moyenne énoncée par [iergo dans son article](https://blocnotes.iergo.fr/articles/comprendre-le-tjm-dun-freelance/) :

> Un freelance facture entre 120 et 150 jours par an, pour 210 jours travaillés

Il vous faut donc compter maximum que vous facturerez 10 jours par mois, certainement moins si vous démarrez (courage, c'est normal !).

### Le calcul

A partir de là :
- Soit vous prenez votre somme mensuelle, et vous la divisez par 10 (pour le SMIC, ça ferait 275€/jour, mais comme on a dit qu'on faisait pas en dessous de 400€/jour, on se calera sur 400)
- Soit vous savez que dans votre profession le TJM moyen est plus élevé, et vous vous y calez, pour ne pas tirer les tarifs de la profession vers le bas :) Si vous ne connaissez pas les tarifs pratiqués, n'hésitez pas à demander autour de vous, on gagne tous à discuter de nos pratiques !

### Démarrer bas puis augmenter ses tarifs ?

On peut être tenté·es de démarrer avec un TJM bas pour se faire un portefeuille de clients, en se disant qu'on augmentera plus tard.

Ce n'est pas une stratégie que nous vous conseillons : Les clients qui sont habitués à un tarif bas nous prennent souvent beaucoup plus d'énergie et sont rarement près à nous suivre lorsque nous augmentons nos prix. Dans le doute, commencez d'abord à un tarif un peu plus haut, et si vous ne trouvez pas de clients au bout de nombreux mois de recherche et/ou que les autres indépendants de votre secteur vous conseillent de baisser vos tarifs, faites-le à ce moment là.

## Conclusion

Si vous êtes arrivés jusqu'ici, bravo, vous avez fait un gros travail d'éclaircissement sur une partie qui fait flipper bon nombre de personnes !

Vous devriez maintenant avoir les 4 chiffres importants :

- Votre **salaire net après impôts** (étape 1)
- Le **coût total employeur** de ce salaire (étape 2)
- La **somme d'argent que vous devez facturer mensuellement**, qui est la somme du coût total employeur + les économies pour faire face au risque (environ 1000€/mois) (étape 3)
- Et **votre TJM** ou tarif-jour (le tarif que vous annoncerez à vos clients)

Félicitations !
